<?php
namespace includes\admin\internal\TasteAmazon;

class Exception extends \Exception
{
  //LOGIN
  const MESSAGE_1 = 1;
  const EMPTY_LOGIN = 'Brugernavn eller adgangskode feltet er tomt.';

  const MESSAGE_2 = 1;
  const WRONG_LOGIN = 'Brugernavn eller adgangskode er forkert.';

  //PRODUCT
  const MESSAGE_3 = 3;
  const EMPTY_PRODUCT_FORM = 'Et eller flere felter er tomme.';

  //GALLERY
  const MESSAGE_7 = 7;
  const EMPTY_GALLERY_TITLE = 'Title feltet er tomt.';

  const MESSAGE_8 = 8;
  const EMPTY_GALLERY_IMAGE = 'Der er ikke uploadet noget billede.';
}
